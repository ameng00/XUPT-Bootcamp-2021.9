1. 创建套接字

```c
int socket(int domain, int type, int protocol);
```

- domain: 协议族
- type：套接字类型
- protocol：协议类型（0由OS选择协议）
- 返回值：套接字（文件描述符）

2. 绑定IP地址和端口号

```C
int bind(int sockfd, struct sockaddr* addr, int addrlen);
```

- sockfd：套接字
- addr：地址结构体
- addrlen：地址长度
- 返回值：成功返回0，失败返回-1

IPv4地址结构：

```C
struct sockaddr_in
{
    unsigned short sin_family; //地址族
    unsigned short sin_port;   //以网络字节序存储的端口号
    struct in_addr sin_addr;   //以网络字节序存储的IPv4地址
};

struct in_addr
{
    uint32_t s_addr;  //使用32位整型保存IP地址
};
```

3. 将套接字设置为监听模式

```C
int listen(int sockfd, int backlog);
```

- sockfd：套接字
- backlog：等待连接的客户端最大数量

- 返回值：成功0，失败-1

4. 等待客户端连接

```c
int accept(int sockfd, struct sockaddr* caddr, socklen_t addrlen);
```

- sockfd：监听套接字
- caddr：客户端地址
- addrlen：客户端地址长度
- 返回值：成功返回连接套接字，失败返回-1

