## Pthread API

头文件：<pthread.h>

1. 创建线程

```C
int pthread_create(pthread_t *thread, const pthread_attr_t *attr,
                          void *(*start_routine) (void *), void *arg);
```

thread：线程ID的指针

attr：线程属性的指针（使用pthread_attr_init进行初始化）

start_routine：线程处理函数

arg：传递给线程处理函数的参数

2. 设置分离线程

```C
int pthread_attr_setdetachstate(pthread_attr_t *attr, int detachstate);
```

3. 互斥量加解锁

```C
int pthread_mutex_lock(pthread_mutex_t *mutex);
int pthread_mutex_unlock(pthread_mutex_t *mutex); 
```

